const divWhereToInsert = document.getElementById("div-post-entries");
const titleInput = document.getElementById("txt-title");
const bodyInput = document.getElementById("txt-body");
const form = document.getElementById("form-add-post");
const updateForm = document.getElementById("form-edit-post");

const allPosts = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");
    const data = await response.json();
    posts(data);
};

allPosts();

const posts = (data) => {
    let posts = "";
    data.forEach((el) => {
        posts += `<div id="post-${el.id}">
        <h3 id="title-${el.id}">${el.title}</h3>
        <p id="body-${el.id}">${el.body}</p>
        <button onclick="editPost(${el.id})">Edit</button>
        <button onclick="deleteHandler(${el.id})">Delete</button>
        </div>`;
    });
    divWhereToInsert.innerHTML = posts;
};

const addPost = async (event) => {
    event.preventDefault();
    const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: titleInput.value,
            body: bodyInput.value,
            id: 2,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    });
    const data = await response.json();
    console.log(data);

    document.getElementById("txt-title").value = null;
    document.getElementById("txt-body").value = null;
};

form.addEventListener("submit", (event) => {
    addPost(event);
});

const editPost = (id) => {
    let title = document.querySelector(`#title-${id}`).innerHTML;
    let body = document.querySelector(`#body-${id}`).innerHTML;

    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

const updatePost = async (event) => {
    event.preventDefault();

    const id = document.getElementById("txt-edit-id");
    const title = document.getElementById("txt-edit-title");
    const body = document.getElementById("txt-edit-body");

    const response = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}`,
        {
            method: "PATCH",
            body: JSON.stringify({
                title: title.value,
                body: body.value,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        }
    );
    const data = await response.json();
    console.log(data);

    document.getElementById("txt-edit-title").value = null;
    document.getElementById("txt-edit-body").value = null;
    document.getElementById("btn-submit-update").setAttribute("disabled", true);
};

updateForm.addEventListener("submit", (event) => {
    updatePost(event);
});

const deleteHandler = async (id) => {
    try {
        const response = await fetch(
            `https://jsonplaceholder.typicode.com/posts/${id}`,
            {
                method: "DELETE",
            }
        );
        const deletedPost = await response.json();
        console.log(deletedPost);

        document.getElementById(`post-${id}`).remove();
    } catch (error) {}
};
